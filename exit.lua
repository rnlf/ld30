Exit = Entity:extend()

function Exit:new(state, x, y, w, h)
  Entity.new(self, state)
  self.size.x = w
  self.size.y = h
  self.pos.x = x
  self.pos.y = y
  self.shape = self.state.collider:addRectangle(x,y,w,h)
  self.shape.entity = self
  self.state.collider:setPassive(self.shape)
  self.gravity = 0
end


function Exit:warpTo(x,y)
  self.pos.x = x - self.size.x / x
  self.pos.y = y - self.size.y / y
end

function Exit:collide(other)
  if other:is(Player) then
    other:finished()
    G.sounds.exit:play()
  end
end
