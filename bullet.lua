Bullet = Entity:extend()

function Bullet:new(state, type)
  Entity.new(self, state)

  if not type then
    self:sprite("data/bullet.png", 6, 3, {{'idle', {0}, 1}})
  elseif type == "fireball" then
    self:sprite("data/fireball.png", 7, 5, {{'idle', {0}, 4}})
  end
  self:play('idle')
  self.drag.x = 0
  self.drag.y = 0
  self.vmax = 1000
  self.gravity = 0

  self.shape = self.state.collider:addRectangle(0,0,self.size.x, self.size.y)
  self.shape.entity = self
  self.bounce = 0.1

  if not type then
    self.state.collider:addToGroup('player', self.shape)
  elseif type == "fireball" then
    self.state.collider:addToGroup('enemies', self.shape)
  end
  self.layer = 30
  self.damage = 10
  self.type = type
end


function Bullet:groundCollision()
  self:kill()
end


function Bullet:collide(other)
  if other.health and other.health > 0 then
    if not self.type then
      G.sounds.hit:play()
    else
      G.sounds.fireball_hit:play()
    end
    other:hurt(self.damage)
    other.vel.x = self.vel.x * 0.3
    self:kill()
  end
end
