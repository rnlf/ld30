local PostEffect = require 'lust.post_effect'

ScaleEffect = PostEffect:extend()

function ScaleEffect:new()
  ScaleEffect.super.new(self)
end


function ScaleEffect:draw(canvas)
  local oldmin, oldmag = canvas:getFilter()
  canvas:setFilter('nearest', 'nearest')
  love.graphics.draw(canvas, 0, 0, 0, 4, 4)
  canvas:setFilter(oldmin, oldmag)
end
