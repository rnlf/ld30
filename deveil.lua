require 'mob'

Deveil = Mob:extend()
Deveil.maxHealth = 200

function Deveil:new(state)
  Mob.new(self, state)

  self:sprite("data/deveil.png", 9, 14, {
    {'idle', {0,1}, 2},
    {'walk', {3,4,5,6,7,8,9,10,11,12}, 8},
    {'dead', {13}, 1}
  })

  self:play('walk')

  self.shape = self.state.collider:addRectangle(0,0,self.size.x, self.size.y)
  self.shape.entity = self
  self.state.collider:addToGroup('enemies', self.shape)
  self.health = Deveil.maxHealth

  self.firetimeout = 0.75
  self.turntimeout = 0.5
  self.speed = love.math.random() * 100 + 150
end


function Deveil:update(dt)
  Mob.update(self, dt)

  if self.health <= 0 then
    return
  end

  self.firetimeout = self.firetimeout - dt
  self.turntimeout = self.turntimeout - dt

  local nearestPlayer = self.state.entities:findNearest(function(e)
      return e:is(Player)
    end, self.pos.x, self.pos.y)

  local sdist = nearestPlayer.pos.x - self.pos.x
  local dist = math.abs(sdist)
  
  if (dist > 120 or math.abs(nearestPlayer.pos.y - self.pos.y) > 40) and self.health == Deveil.maxHealth then
    self.acc.x = 0
    return
  elseif dist < 200 and tools.sig(sdist) == tools.sig(self.scale.x) then
    if self.firetimeout <= 0 then
      local bullet = Bullet(self.state, "fireball")
      bullet:warpTo(self.pos.x + (self.scale.x < 0 and 0 or (self.size.x- bullet.size.x)), self.pos.y + 2)
      local angle = love.math.random() * 0.2 - 0.1
      
      bullet.vel.x = self.scale.x * 200 * math.cos(angle) + self.vel.x
      bullet.vel.y = 200 * math.sin(angle) 
      bullet.scale.x = self.scale.x
      bullet.damage = 7.5
      G.sounds.fireball:play()
      self.state.entities:add(bullet)
      self.firetimeout = 1.5 + love.math.random()
    end
  end

  if self.turntimeout <= 0 and math.abs(self.lastpos.x - self.pos.x) < 0.1 and math.abs( self.vel.x) > 0 then
    self.acc.x = -tools.sig(self.vel.x) * 100
    self.scale.x = -self.scale.x
    self.turntimeout = 1
  else
    if tools.sig(sdist) ~= tools.sig(self.scale.x) then
      local sig = tools.sig(sdist)
      self.acc.x = sig * self.speed
      self.scale.x = sig
    end
  end
end


function Deveil:ondie()
  G.sounds.killdeveil:play()
  self:play('dead')
  self.acc.x = 0
  self.acc.y = 0
end


function Deveil:hurt(amount)
  Mob.hurt(self, amount)

  self.turntimeout = 0.5
end
