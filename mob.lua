Mob = Entity:extend()

function Mob:new(state)
  Entity.new(self, state)
  self.bounce = 0
  self.health = 100
  self.layer = 10
end

function Mob:onGround()
  local a = self.pos.x
  local b = self.pos.y + self.size.y
  local c = self.pos.x + self.size.x
  local d = self.pos.y + self.size.y + 1
  self.footshape:moveTo(self.pos.x + self.size.x / 2 - 0.5, self.pos.y + self.size.y)
  for shape in pairs(self.state.collider:shapesInRange(a,b,c,d)) do
    if shape.islevel then
      if shape:collidesWith(self.footshape) then
        return true
      end
    end
  end
  return false
end


function Mob:jump()
end


function Mob:die()
  Entity.die(self)
  if self.footshape then
    self.state.collider:remove(self.footshape)
  end
end


function Mob:ondie()
  self:kill()
end


function Mob:hurt(amount)
  self.health = self.health - amount
  self.color = {r = 128, g = 0, b = 0, alpha = 0}
  self.state.tween:to(self.color, 0.2, {r = 255, g = 255, b = 255, alpha = 255})
  if self.health <= 0 then
    self:ondie()
  end
end
