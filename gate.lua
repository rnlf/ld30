Gate = Entity:extend()

function Gate:new(state, type)
  Entity.new(self, state)

  self.texture = G.images:load("data/gate.png")
  self.parts = {}
  for i = 0, 7 do
    self.parts[i+1] = G.quads:make((type-1) * 6, i, 6, 8 - i, self.texture:getWidth(), self.texture:getHeight())
  end

  self.cap = G.quads:make((type-1)*6, 8, 6, 1, self.texture:getWidth(), self.texture:getHeight())
  self.gravity = 0
  self.opening = 0

  self.tween = flux.group()
  self.type = type

  self.layer = 40

end

function Gate:prepare()
  self.collshape = self.state.collider:addRectangle(self.pos.x, self.pos.y, self.width, self.height - self.opening)
  self.state.collider:setPassive(self.collshape)
  self.state.collider:addToGroup("level", self.collshape)
  self.collshape.islevel = true
end


function Gate:update(dt)
  Entity.update(self, dt)
  self.tween:update(dt)
end


function Gate:draw()
  love.graphics.setColor(255,255,255,255)

  local height = math.floor(self.height - self.opening)
  local texture = self.texture
  local parts = self.parts
  local x = self.pos.x
  local y = self.pos.y

  love.graphics.draw(texture, self.cap, x, y + height - 1)
  local count = math.floor((height - 1) / 8)
  for i = 1, count do
    love.graphics.draw(texture, parts[1], x, y + height - i * 8 - 1 )
  end

  local rest = 9 - (height - (count * 8 + 1))
  if rest ~= 9 then
    love.graphics.draw(texture, parts[rest], x, y)
  end
end

function Gate:open()
  if self.opening == 0 then
    G.sounds.gate:setLooping(true)
    G.sounds.gate:play()
    self.tween:to(self, self.height / 40.0, {opening = self.height - 1})
      :ease('linear')
      :onupdate(function()
          self.state.collider:remove(self.collshape)
          self:prepare()
        end)
      :oncomplete(function()
          G.sounds.gate:stop()
          self.state.collider:remove(self.collshape)
          self.collshape = nil
        end)
  end

end
