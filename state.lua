require 'level'
require 'entityfactory'
require 'camera'
require 'text'

State = Object:extend()
State.intro = 'Press [R] at any time to restart. ' ..
      'Use [Curser keys] to move and jump. ' ..
      'Press [Enter] to pick up or drop items. ' ..
      'Press [Ctrl] to use the item in your hand. ' ..
      'Using a weapon alone fires it, using it with an upgrade terminal upgrades the weapon. ' ..
      'Use keycards with terminals to open doors. ' ..
      "Drop items into a teleporter to teleport it to your sibling's world. " ..
      'Press [Tab] to switch to your sibling. ' ..
      '"Jessie and Jack" made in 48 hours for Ludum Dare 30 by rnlf.'

function State.entityGroundCollision(dt, a, dx, dy)
  a:warp(dx, dy)
  a:groundCollision()
  a.drag.x = 6
  if dx > 0 and a.vel.x < 0 then
    a.vel.x = -a.vel.x * a.bounce
  end

  if dx < 0 and a.vel.x > 0 then
    a.vel.x = -a.vel.x * a.bounce
  end

  if dy > 0 and a.vel.y < 0 then
    a.vel.y = -a.vel.y * a.bounce
  end

  if dy < 0 and a.vel.y > 0 then
    a.vel.y = -a.vel.y * a.bounce
  end


end


function State:new(levelfile)
  local collide = function(dt, a, b, x, y)
    if a.entity and b.islevel then
      self.entityGroundCollision(dt, a.entity, x, y)
    end

    if b.entity and a.islevel then
      self.entityGroundCollision(dt, b.entity, -x, -y)
    end

    if a.entity and b.entity then
      if a.entity:is(Player) and b.entity.carriable then
        a.entity.currentTarget = b.entity
      elseif b.entity:is(Player) and a.entity.carriable then
        b.entity.currentTarget = a.entity
      else
        b.entity:collide(a.entity)
        a.entity:collide(b.entity)
      end
    end
  end

  local collision_stop = function(dt, a, b)
    if a.entity and b.entity then
      if a.entity.currentTarget and a.entity.currentTarget == b.entity then
        a.entity.currentTarget = nil
      end

      if b.entity.currentTarget and b.entity.currentTarget == a.entity then
        b.entity.currentTarget = nil
      end

      a.entity:uncollide(b.entity)
      b.entity:uncollide(a.entity)
    end
  end

  self.collider = HC(32, collide, collision_stop)

  self.level = Level(self, levelfile)
  self.entities = Group()

  self.players = {}

  for _, e in pairs(self.level.entitylayer.objects) do
    self.entities:add(makeEntity(e, self))
  end

  self.activePlayer = 0

  self.camera = Camera(self)
  self.camera:track(self.players[0])

  self.flashColor = {0,0,0}
  self.flashAlpha = 255

  self.tween = flux.group()
  self.tween:to(self, 1, {flashAlpha = 0}):delay(0.5)

  self.shakeAmount = 0
  self.text = Text()


  self.hasmoved = false
  self.intropos = G.width

  self.introlen = G.font:getWidth(State.intro)
end


function State:update(dt)
  if not self.hasmoved then
    self.intropos = self.intropos - 80 * dt
    if self.intropos < -self.introlen then
      self.intropos = G.width
    end
  end


  if (self.players[self.activePlayer].done and  self.players[1-self.activePlayer].done)
    or self.players[0].health <= 0 or self.players[1].health <= 0 then
    self.flashColor = {0,0,0}
    self.flashAlpha = 0
    self.tween:to(self, 2, {flashAlpha=255})
      :oncomplete(function()
          self.draw = function(self)
            love.graphics.setBackgroundColor(0,0,0)
            love.graphics.clear()
            local dead = -1
            if self.players[0].health <= 0 then
              dead = 0
            elseif self.players[1].health <= 0 then
              dead = 1
            end
            if dead ~= -1 then
              love.graphics.setColor(255,0,0,255)
              love.graphics.printf( ((dead == 0) and "Jessie" or "Jack") .. " has died.", 0, 30, G.width, 'center')
              love.graphics.setColor(255,255,255,255)
              love.graphics.printf("[R] to retry", 0, 70, G.width, 'center')
            else
              love.graphics.setColor(0,255,0,255)
              love.graphics.printf("Jessie and Jack survived!", 0, 30, G.width, 'center')
              love.graphics.setColor(255,255,255,255)
              love.graphics.printf("Thanks for playing!", 0, 70, G.width, 'center')
              love.graphics.setColor(128,128,128,255)
              love.graphics.printf("Made in 48 hours for Ludum Dare 30", 0, 100, G.width, 'center')
              love.graphics.printf("Made by rnlf", 0, 120, G.width, 'center')
            end
          end
      end)

    self.update = function(self, dt)
      self.tween:update(dt)
    end
  end

  if self.players[self.activePlayer].done and not self.players[1-self.activePlayer].done then
    self.activePlayer = 1 - self.activePlayer
    self:flashScreen(255,255,255,1,function()
      self.camera:track(self.players[self.activePlayer])
    end)
  end

  if love.keyboard.isDown('tab') then
    if not self.wasSwitching and not self.players[1-self.activePlayer].done then
      local r = 255 
      local g = 32
      if self.activePlayer == 0 then
        r = 32 
        g = 255
      end
      G.sounds.warp:play()
      self:flashScreen(r,g,0,0.25, function()
        self.players[self.activePlayer].acc = {x = 0, y = 0}
        self.activePlayer = 1 - self.activePlayer
        self.camera:track(self.players[self.activePlayer])
      end)
      self:shake(40, 0.5)
    end

    self.wasSwitching = true
  else
    self.wasSwitching = false
  end

  if not self.players[self.activePlayer].done then
    local dir = 0
    if love.keyboard.isDown('left') then
      dir = dir - 1
      self.hasmoved = true
    end
    
    if love.keyboard.isDown('right') then
      dir = dir + 1
      self.hasmoved = true
    end
    
    local using = love.keyboard.isDown('lctrl') or love.keyboard.isDown('rctrl')

    self.players[self.activePlayer]:walk(dir, using)

    if love.keyboard.isDown('up') then
      self.players[self.activePlayer]:jump()
    end

    if love.keyboard.isDown('return') then
      if not self.wasPicking then
        self.players[self.activePlayer]:pickupOrDrop()
      end
      self.wasPicking = true
    else
      self.wasPicking = false
    end

    if using then
      self.players[self.activePlayer]:use()
    end

  end

  self.tween:update(dt)
  local itercount = math.ceil(90 * dt)
  for i = 1, itercount do 
    self.entities:update(dt/itercount)
    self.level:update(dt/itercount)
    self.collider:update(dt/itercount)
  end

  self.camera:setDstOffset(0.3*self.players[self.activePlayer].vel.x, self.players[self.activePlayer].vel.y*0.05)

  self.camera:update(dt)

  self.text:update(dt)

end


function State:draw()
  if self.activePlayer == 0 then
    love.graphics.setBackgroundColor(91,38,59)
  else
    love.graphics.setBackgroundColor(32,32,32)
  end
  love.graphics.clear()
  love.graphics.setColor(255,255,255,255)
  self.camera:setup()
  love.graphics.translate(math.floor(love.math.random() * self.shakeAmount), math.floor(love.math.random() * self.shakeAmount))
  self.level:draw()
  self.entities:draw()

  self.text:draw()

  love.graphics.origin()
  love.graphics.setBlendMode('alpha')
  love.graphics.setColor(self.flashColor[1], self.flashColor[2], self.flashColor[3], self.flashAlpha)
  love.graphics.rectangle('fill', 0, 0, G.width, G.height)

  love.graphics.setColor(0,0,0,128)
  love.graphics.rectangle('fill', 0,0, G.width,18)

  love.graphics.setColor(255,0,0, (self.activePlayer == 0) and 255 or 64)
  love.graphics.printf("Jessie", 35, 0, 0, 'right')
  love.graphics.rectangle('fill', 37, 3, math.floor((G.width - 40) * self.players[0].health / 100.0), 4)

  love.graphics.setColor(0, 255,0, (self.activePlayer == 1) and 255 or 64)
  love.graphics.printf("Jack", 35, 8, 0, 'right')
  love.graphics.rectangle('fill', 37, 11, math.floor((G.width - 40) * self.players[1].health / 100.0), 4)

  love.graphics.setColor(255,255,255,255)

  if not self.hasmoved then
    love.graphics.setColor(0,0,0,192)
    love.graphics.rectangle('fill', 0,G.height - 10, G.width, 10)
    love.graphics.setColor(255,255,255,255)
    love.graphics.print(State.intro, self.intropos,  G.height - 10)
  end

end


function State:flashScreen(r, g, b, t, onflashed, alpha)
  self.flashColor[1] = r
  self.flashColor[2] = g
  self.flashColor[3] = b
  self.flashAlpha = 0
  self.tween:to(self, t / 2, {flashAlpha = alpha or 255})
    :oncomplete(function()
      if onflashed then
        onflashed()
      end
      self.tween:to(self, t / 2, {flashAlpha = 0})
    end)
end


function State:shake(amount, time)
  self.shakeAmount = amount
  self.tween:to(self, time, {shakeAmount = 0})
end
