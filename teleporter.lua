Teleporter = Entity:extend()

function Teleporter:new(state, i)
  Entity.new(self, state)

  self:sprite("data/teleporter.png", 24, 24, {
    {'idle', {0+5*i,1+5*i,2+5*i,3+5*i,4+5*i,3+5*i,2+5*i,1+5*i}, 10}
  })

  self.shape = state.collider:addCircle(0,0,10)
  state.collider:addToGroup('level', self.shape)
  state.collider:setPassive(self.shape)
  self.shape.entity = self

  self:play('idle')

  self.gravity = 0

  self.layer = 5
  self.type = i
end


function Teleporter:warpTo(x,y)
  Entity.warpTo(self, x - self.size.x / 2, y - self.size.y / 2)
end


function Teleporter:collide(other)
  if other.teleportable then
    local partner = self.state.entities:find(function(a)
      return a:is(Teleporter) and a ~= self and a.type == self.type
    end)

    if not partner then
      print("warning, partner teleporter not found")
    else
      G.sounds.teleport:play()
      other.teleportable = false
      local vel = { x = other.vel.x, y = other.vel.y }
      other.vel.x = other.vel.x * 0.1
      other.vel.y = other.vel.y * 0.1
      local grav = other.gravity
      other.gravity = 0

      self.state.tween:to(other.color, 1, {r = 255, b = 255, b = 255, a = 0})
        :oncomplete(function()
          other:warpTo(partner.pos.x + partner.size.x / 2 , partner.pos.y + partner.size.y / 2)
          self.state.tween:to(other.color, 1, {r = 255, g = 255, b = 255, a = 255})
            :oncomplete(function()
              other.vel.x = vel.x
              other.vel.y = vel.y
              other.gravity = grav
            end)
        end)
    end
  end
end
