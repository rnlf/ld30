Shell = Entity:extend()

function Shell:new(state)
  Entity.new(self, state)

  self:sprite('data/shell.png', 4, 3, {{'idle', {0}, 1}})
  self:play('idle')

  self.shape = state.collider:addRectangle(0,0,self.size.x, self.size.y)
  self.shape.entity = self
  self.bounce = 0.99
  state.collider:addToGroup("shells", self.shape)

  self.timeout = 5
  self.tween = flux.group()

end


function Shell:update(dt)
  Entity.update(self, dt)

  self.tween:update(dt)

  if self.timeout >= 0 then
    self.timeout = self.timeout - dt
    local vrx = self.pos.x - self.lastpos.x
    local vry = self.pos.y - self.lastpos.y
    if tools.sq(vrx) + tools.sq(vry) > 0.01 then
      self.timeout = 1 
    end

    if self.timeout <= 0 then
      self.gravity = 0
      self.vel.x = 0
      self.vel.y = 0
      self.state.collider:remove(self.shape)
      self.tween:to(self.color, 5, {a = 0})
        :oncomplete(function() self:kill() end)
    end
  end
end
