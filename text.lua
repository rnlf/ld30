Text = Object:extend()

function Text:new()
  self.blobs = {}
  self.tween = flux.group()
end


function Text:update(dt)
  for t in pairs(self.blobs) do
    if t.vx then
      t.x = t.x + t.vx * dt
    end

    if t.vy then
      t.y = t.y + t.vy *dt
    end

    t.timeout = t.timeout - dt
    if t.timeout <= 0 then
      self.tween:to(t, 2, {alpha = 0.0})
        :oncomplete(function() self.blobs[t] = nil end)
    end
  end

  self.tween:update(dt)
end


function Text:draw()
  love.graphics.setFont(G.font)
  local h = G.font:getHeight()
  for t in pairs(self.blobs) do
    local w = G.font:getWidth(t.text)
    love.graphics.setColor(0,0,0,128*t.alpha)
    love.graphics.rectangle('fill', t.x - w / 2 - 2, t.y - h / 2, w+4, h+4)
    love.graphics.setColor(255,255,255,255*t.alpha)
    love.graphics.print(t.text, t.x - w / 2, t.y - h / 2 +2 )
  end
end


function Text:add(text, x, y, timeout, vx, vy)
  local blob = {text = text, x = x, y = y, timeout = timeout, alpha = 1.0, vx = vx, vy = vy}
  self.blobs[blob] = blob
end
