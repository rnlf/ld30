Entity = Object:extend()

function Entity:new(state)
  self.state = state
  self.pos = { x = 0, y = 0 }
  self.vel = { x = 0, y = 0 }
  self.acc = { x = 0, y = 0 }
  self.lastpos = { x = 0, y = 0 }
  self.size = {x = 16, y = 16 }
  self.gravity = 1000
  self.drag = { x = 2, y = 4 }
  self.bounce = 0

  self.scale = { x = 1, y = 1 }
  self.collisionOffset = { x = 0, y = 0 }

  self.vmax = 400

  self.layer = 0

  self.color = {r = 255,g = 255, b = 255, a = 255}
end


function Entity:sprite(filename, w, h, anims)
  self.image = G.images:load(filename)
  self.anims = {}
  local iw, ih = self.image:getDimensions()
  local fpr = math.floor(iw / w)

  for _, a in ipairs(anims) do
    local f = {}
    for i, cf in ipairs(a[2]) do
      local x = cf % fpr
      local y = math.floor(cf / fpr)
      f[i] = G.quads:make(x * w, y * h, w, h, iw, ih)
    end

    self.anims[a[1]] = {
      frames = f,
      fps = a[3]
    }
  end

  self.size.x = w
  self.size.y = h
end


function Entity:update(dt)
  if self.anim then
    local oldframe = self.curframe
    self.curframe = math.fmod(self.curframe + dt * self.anim.fps, #self.anim.frames)

    if oldframe > self.curframe then
      if self.anim ~= self.nextAnim then
        self.anim = self.nextAnim
        self.curframe = 0
      end
    end
  end
  
  self.lastpos.x = self.pos.x
  self.lastpos.y = self.pos.y
  if not self.carriable or not self.carrier then
    local accy = self.acc.y + self.gravity - self.vel.y * self.drag.y
    local accx = self.acc.x - self.vel.x * self.drag.x
    self.vel.y = self.vel.y + accy * dt
    self.vel.x = self.vel.x + accx * dt

    self.pos.x = self.pos.x + dt * self.vel.x
    self.pos.y = self.pos.y + dt * self.vel.y

    if math.sqrt(self.vel.x*self.vel.x + self.vel.y * self.vel.y) > self.vmax then
      self.vel.x, self.vel.y = tools.scale(self.vel.x, self.vel.y, self.vmax)
    end
  end

  self:updateShape()
end


function Entity:play(anim, noreset)
  self.anim = self.anims[anim]
  self.nextAnim = self.anim
  if not noreset or self.curframe > #self.anim.frames then
    self.curframe = 0
  end
end


function Entity:draw()
  if self.anim then
    local p = self.pos
    love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.a)
    love.graphics.draw(self.image, self.anim.frames[math.floor(self.curframe) + 1], math.floor(p.x + ((self.scale.x < 0) and self.size.x or 0)), math.floor(p.y), 0, self.scale.x, self.scale.y)
  end
  --[[
  if self.shape then
    self.shape:draw()
  end
  ]]

end


function Entity:warpTo(x,y)
  self.pos = { x = x, y = y }
  self.lastpos = { x = x, y = y }
  self:updateShape()
end

function Entity:warp(x,y)
  self:warpTo(self.pos.x + x, self.pos.y + y)
end


function Entity:updateShape()
  if self.shape then
    self.shape:moveTo(self.pos.x + self.size.x / 2 - 0.5
                      + self.collisionOffset.x * self.scale.x,
                      self.pos.y + self.size.y / 2 - 0.5
                       + self.collisionOffset.y * self.scale.y)
  end
end


function Entity:collide(other)

end


function Entity:uncollide(other)

end


function Entity:kill()
  self.dead = true
end


function Entity:prepare()

end


function Entity:use()
end


function Entity:groundCollision()
  
end


function Entity:die()
  if self.shape then
    self.state.collider:remove(self.shape)
  end
end
