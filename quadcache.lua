QuadCache = Object:extend()

function QuadCache:new()
  self.quads = {}
end


function QuadCache:make(a,b,c,d,e,f)
  if not self.quads[a] then
    self.quads[a] = {}
  end

  local ta = self.quads[a]
  if not ta[b] then
    ta[b] = {}
  end

  local tb = ta[b]
  if not tb[c] then
    tb[c] = {}
  end

  local tc = tb[c]
  if not tc[d] then
    tc[d] = {}
  end

  local td = tc[d]
  if not td[e] then
    td[e] = {}
  end

  local te = td[e]
  if not te[f] then
    te[f] = love.graphics.newQuad(a,b,c,d,e,f)
  end

  return te[f]
end
