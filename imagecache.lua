ImageCache = Object:extend()

function ImageCache:new()
  self.images = {}
end


function ImageCache:load(filename)
  if not self.images[filename] then
    self.images[filename] = love.graphics.newImage(filename)
  end

  return self.images[filename]
end
