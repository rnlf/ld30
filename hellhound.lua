require 'mob'

Hellhound = Mob:extend()
Hellhound.maxHealth = 60

function Hellhound:new(state)
  Mob.new(self, state)

  self:sprite("data/hellhound.png", 8, 8, {
    {'idle', {0, 1, 2}, 4},
    {'run', {3,4,5,6}, 8},
    {'dead', {7}, 1}
  })

  self:play('run')

  self.shape = self.state.collider:addRectangle(0,0,self.size.x, self.size.y+2)
  self.shape.entity = self
  self.collisionOffset.y = -1

  self.footshape = state.collider:addRectangle(0,0,self.size.x-3,1)
  self.state.collider:setPassive(self.footshape)

  self.state.collider:addToGroup('enemies', self.shape, self.footshape)

  self.jumpdelay = 0
  self.turndelay = 0
  self.foodelay = 0

  self.vmax = love.math.random() * 100 + 200
  self.racc = love.math.random() * 100 + 300
  self.health = Hellhound.maxHealth
  self.hittimeout = 0
end


function Hellhound:update(dt)

  Mob.update(self, dt)

  if self.health <= 0 then
    return
  end


  local nearestPlayer = self.state.entities:findNearest(function(e)
      return e:is(Player)
    end, self.pos.x, self.pos.y)


  self.jumpdelay = self.jumpdelay - dt
  self.turndelay = self.turndelay - dt
  self.foodelay = self.foodelay - dt
  self.hittimeout = self.hittimeout - dt

  local sdist = nearestPlayer.pos.x - self.pos.x
  local dist = math.abs(sdist)

  if (dist > 80 or math.abs(nearestPlayer.pos.y - self.pos.y) > 80) and self.health == Hellhound.maxHealth then
    self.acc.x = 0
    return
  elseif dist < 80 and self.turndelay < 0 then
    self.turndelay = 0.5 + love.math.random()
    if nearestPlayer.pos.x > self.pos.x then
      self.acc.x = self.racc
      self.scale.x = 1
    else
      self.acc.x = -self.racc
      self.scale.x = -1
    end
  end


  if self.jumpdelay < 0 then
    if self:onGround() and tools.sig(sdist) == tools.sig(self.vel.x) then
      self.jumpdelay = love.math.random() * 1 + 0.5 
      self.vel.y = -200 - 250 * love.math.random()
      self.vel.x = self.vel.x * 2 
    end
  end
end


function Hellhound:jump()
end


function Hellhound:collide(other)
  if self.health > 0 and other:is(Player) and self.hittimeout <= 0 then
    other:hurt(2.5)
    self.hittimeout = 1
  end
end


function Hellhound:ondie()
  self:play('dead')
  G.sounds.killhellhound:play()
  self.acc.x = 0
  self.acc.y = 0
end
