require 'shell'
require 'bullet'

Gun1 = Entity:extend()

Gun1.upgrades = {
  {rate = 0.13, bullets = 1, damage = 10, vbullet = 400, text = ""},
  {rate = 0.13, bullets = 3, damage = 10, vbullet = 400, text = "More Bullets"},
  {rate = 0.1, bullets = 3, damage = 10, vbullet = 400, text = "Higher Rate of Fire"},
  {rate = 0.1, bullets = 3, damage = 15, vbullet = 400, text = "More Damage"},
  {rate = 0.1, bullets = 3, damage = 15, vbullet = 600, text = "Faster Bullets"}
}

function Gun1:new(state, i)
  Entity.new(self, state)

  self.type = i

  self:sprite("data/gun1.png", 16, 9, {
    {'idle', {0}, 1},
    {'fire', {1,0}, 30}
  })

  self:play('idle')

  self.shape = state.collider:addRectangle(0, 0,9,7 )
  self.collisionOffset = {x = -2, y = -1}
  self.shape.entity = self

  self.carriable = true

  self.drag.x = 1

  self.carryOrigin = { x = 0, y = 7 }
  self.timeout = 0
  self.tween = flux.group()
  self.layer = 20
  self.power = 1
end


function Gun1:collide(other)
  if other:is(Upgrade) and other.used == 0 then
    self.target = other
  end
end


function Gun1:uncollide(other)
  if self.target == other then
    self.target = nil
  end
end


function Gun1:use()
  if self.target then
    self.target:useWith(self)
    return
  end


  if self.timeout <= 0 then
    G.sounds.shoot:play()
    local u = Gun1.upgrades[self.power]
    self.timeout = u.rate
    self:play('fire')
    self.nextAnim = self.anims['idle']

    for b = 1, u.bullets do
      local bullet = Bullet(self.state)
      bullet:warpTo(self.pos.x + (self.scale.x < 0 and 0 or (self.size.x- bullet.size.x)), self.pos.y + 2)
      local angle = love.math.random() * 0.2 - 0.1 + (b - math.ceil(u.bullets/2)) * 0.15
      
      bullet.vel.x = self.scale.x * u.vbullet * math.cos(angle) + 0*self.carrier.vel.x
      bullet.vel.y = u.vbullet * math.sin(angle) 
      bullet.scale.x = self.scale.x
      bullet.damage = u.damage
      self.state.entities:add(bullet)
      local shellvx = love.math.random() * 200 - 100
      local shellvy = -love.math.random() * 200 - 100
      local shell = Shell(self.state)
      local cx, cy = self.shape:center()
      shell:warpTo(cx + ((self.scale.x < 0) and shell.size.x / 2 or -shell.size.x / 2) , cy)
      shell.vel.x = shellvx
      shell.vel.y = shellvy
      shell.scale.x = self.scale.x
      self.state.entities:add(shell)
    end

    self.carryOrigin.x = 2.5
    self.tween:to(self.carryOrigin, 0.1, {x = 0})
    self.carrier.vel.x = self.carrier.vel.x - tools.sig(self.scale.x)*5
    self.state:shake(5, 0.2)

  end
end


function Gun1:update(dt)
  Entity.update(self, dt)
  self.timeout = self.timeout - dt
  self.tween:update(dt)
end

function Gun1:upgrade()
  self.power = self.power + 1
  self.state.text:add(Gun1.upgrades[self.power].text, self.pos.x + self.size.x / 2, self.pos.y - 10, 0, 0, -10)
end


