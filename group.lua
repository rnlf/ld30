Group = Entity:extend()

function Group:new()
  Entity.new(self)
  self.children = {}
end


function Group:update(dt)
  local count = 0
  for i, e in pairs(self.children) do
    count = count + 1
    if e.dead then
      e:die()
      self.children[e] = nil
    else
      e:update(dt)
    end
  end
end


function Group:draw()
  local sorted = {}
  for i, e in pairs(self.children) do
    --e:draw()
    table.insert(sorted, e)
  end
  table.sort(sorted, function(a,b) return a.layer < b.layer end)
  for _, e in ipairs(sorted) do
    e:draw()
  end
end


function Group:add(entity)
  self.children[entity] = entity
end


function Group:remove(entity)
  e:die()
  self.children[entity] = nil
end


function Group:find(op)
  for i, e in pairs(self.children) do
    if op(e) then
      return e
    end
  end
end


function Group:findNearest(op, x, y)
  local nearest = nil
  for i, e in pairs(self.children) do
    if op(e) then
      if not nearest then
        nearest = e
      else
        if tools.sq(nearest.pos.x-x) + tools.sq(nearest.pos.y-y) > tools.sq(e.pos.x - x) + tools.sq(e.pos.y - y) then
          nearest = e
        end
      end
    end
  end

  return nearest
end
