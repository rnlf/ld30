require 'mob'

Player = Mob:extend()

function Player:new(state, num)
  Mob.new(self, state)
  self.num = num
  local i = 32 * num
  self:sprite("data/player.png", 8, 16, {
    {'idle', {0+i}, 1},
    {'walk', {1 + i, 2 + i,3+i,4+i,5+i,6+i}, 6},
    {'walk_carry', {7 + i, 8 + i,9+i,10+i,11+i,12+i}, 6},
    {'idle_carry', {13+i}, 1},
    {'jump', {14+i,  15+i}, 6},
    {'jump_carry', {16+i, 17+i}, 6},
    {'fall', {18+i,  19+i}, 6},
    {'fall_carry', {20+i, 21+i}, 6},

  })

  self:play('idle')

  self.shape = state.collider:addRectangle(0,0,self.size.x-2, self.size.y - 3)
  self.shape.entity = self
  self.footshape = state.collider:addRectangle(0,0,self.size.x-3,1)
  self.state.collider:setPassive(self.footshape)

  self.state.collider:addToGroup('player', self.shape, self.footshape)
  self.walkAcc = 600
  self.collisionOffset.y = 1.5

  self.bounce = 0.1
  self.gravity = 850
  self.drag.y = 0

end
 

function Player:walk(dir, strafe)
  self.drag.x = 0
  self.walkAcc = 700

  local amp = 1
  if not self.grounded then
    amp = 0.2
  end
  self.acc.x = dir * self.walkAcc * amp
  if dir ~= 0 and not strafe then
    self.scale.x = dir
  end

  if dir == 0 and self.grounded then
    self.drag.x = 10
  end
end


function Player:jump()
  if self.grounded then
    G.sounds.jump:play()
    self.vel.y = -280
    self.acc.x = self.acc.x * 0.2

  end
end


function Player:update(dt)
  self.grounded = self:onGround()
  Mob.update(self, dt)

  if self.done then
    return
  end

  local carryBob = 1
  local bobframe = math.floor(self.curframe)
  if self.grounded then
    if math.abs(self.vel.x) > 1 then
      if self.carry then
        self:play('walk_carry', true)
        if bobframe == 3 or bobframe == 0 then
          carryBob = 2
        end
      else
        self:play('walk', true)
      end
    else
      if self.carry then
        self:play('idle_carry')
        carryBob = 2 
      else
        self:play('idle')
      end
    end
  else
    local jumpfall = self.vel.y > 0 and "fall" or "jump"
    if self.carry then
      self:play(jumpfall .. '_carry', true)
    else
      self:play(jumpfall, true)
    end
  end

  if self.carry then
    local sub = self.scale.x < 0 and self.carry.size.x or 0

    self.carry.pos.x = self.pos.x + self.size.x / 2 + self.scale.x * (2 - self.carry.carryOrigin.x) - sub
    self.carry.pos.y = self.pos.y + self.size.y / 2 - self.carry.carryOrigin.y + carryBob
    self.carry.scale.x = self.scale.x
  end
end


function Player:pickupOrDrop()
  if self.carry then
    --G.sounds.drop:play()
    self.carry.pos.x = self.pos.x + self.size.x / 2 - self.carry.size.x / 2
    self.carry.pos.y = self.pos.y
    self.carry.vel.x = self.scale.x * 100 + self.vel.x
    self.carry.vel.y = -100 + self.vel.y
    self.carry.carrier = nil
    self.carry.teleportable = true
    self.state.collider:removeFromGroup("level", self.carry.shape)
    self.state.collider:removeFromGroup("player", self.carry.shape)
    self.carry = nil
  elseif self.currentTarget then
    G.sounds.pickup:play()
    self.carry = self.currentTarget
    self.currentTarget = nil
    self.carry.carrier = self
    self.carry.teleportable = false
    --self.state.collider:setGhost(self.carry.shape)
    self.state.collider:addToGroup("level", self.carry.shape)
    self.state.collider:addToGroup("player", self.carry.shape)
  end
end



function Player:draw()
  Mob.draw(self)
--  self.shape:draw()
end


function Player:use()
  if self.carry then
    self.carry:use()
  end
end


function Player:hurt(amount)
  if self.done then
    return
  end
  Mob.hurt(self, amount)
  G.sounds.hurt:play()
  self.state:flashScreen(255,0,0,0.1,nil,128)
  self.state:shake(2*amount, 0.2)
end


function Player:finished()
  self.done = true
end


function Player:ondie()
  Mob.ondie(self)
  G.sounds.die:play()
end
