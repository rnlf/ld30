Key = Entity:extend()

function Key:new(state, i)
  Entity.new(self, state)

  self.type = i

  self:sprite("data/keys.png", 9, 5, {
    {'idle', {i-1}, 1}
  })

  self:play('idle')

  self.shape = state.collider:addRectangle(0, 0, self.size.x, self.size.y)
  self.shape.entity = self

  self.carriable = true

  self.drag.x = 1

  self.carryOrigin = { x = 0, y = 2 }
  self.layer = 25

end


function Key:collide(other)
  if other:is(Terminal) and other.type == self.type then
    self.target =  other
  end
end


function Key:uncollide(other)
  if other == self.target then
    self.target = nil
  end
end


function Key:use()
  if self.target and self.target.access == 0 then
    self.carrier.carry = nil
    self:kill()
    self.target:use()
  end
end
