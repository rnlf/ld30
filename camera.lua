Camera = Object:extend()

function Camera:new(state)
  self.vel = {x = 0, y = 0}
  self.acc = {x = 0, y = 0}
  self.pos = {x = 0, y = 0}
  self.offset = {x = 0, y = 0}
  self.tween = flux:group()
  self.state = state
end


function Camera:warp(x, y)
  self.target = { x = x, y = y }
  self.pos.x = x
  self.pos.y = y
end


function Camera:setup()
  local x = math.floor(self.pos.x - G.width / 2 + self.tracked.size.x / 2)
  local y = math.floor(self.pos.y - G.height / 2 + self.tracked.size.y / 2)

  local border = self.state.level.viewports[self.tracked.num]

  if x < border.x then
    x = border.x 
  end

  if x > border.x + border.width - G.width then
    x = border.x + border.width - G.width
  end

  if y < border.y then
    y = border.y 
  end

  if y > border.y + border.height - G.height then
    y = border.y + border.height - G.height
  end
  love.graphics.origin()
  love.graphics.translate(-x,-y)
                          
end


function Camera:setDstOffset(x,y)
  self.tween:to(self.offset, 0.5,  { x= x })
    :ease('sineout')
end


function Camera:update(dt)
  self.tween:update(dt)
  self.tween:to(self.pos, 3, {y=self.tracked.pos.y})
    :ease('expoout')

  self.pos.x = self.tracked.pos.x + self.offset.x
end


function Camera:track(entity)
  self.tracked = entity
  self:warp(entity.pos.x, entity.pos.y)
end
