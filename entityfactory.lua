require 'player'
require 'teleporter'
require 'key'
require 'terminal'
require 'gate'
require 'hellhound'
require 'gun1'
require 'upgrade'
require 'deveil'
require 'exit'

local makePlayer = function(data, state)
  local p = Player(state, tonumber(data.properties.player))
  state.players[p.num] = p
  return p
end

local function default(Class)
  return function(data, state)
    return Class(state)
  end
end

local makeTeleporter = function(data, state)
  local t = Teleporter(state, tonumber(data.properties.type) - 1)
  return t
end

local makeKey = function(data, state)
  return Key(state, tonumber(data.properties.type))
end

local makeTerminal = function(data, state)
  local terminal =  Terminal(state, tonumber(data.properties.type))

  terminal.target = data.properties.target

  return terminal
end

local makeGate = function(data, state)
  local gate = Gate(state,tonumber(data.properties.type))
  gate.width = data.width
  gate.height = data.height
  
  return gate
end


local makeExit = function(data, state)
  return Exit(state, data.x, data.y, data.width, data.height)
end

local entityFactories = {
  player = makePlayer,
  teleporter = makeTeleporter,
  key = makeKey,
  terminal = makeTerminal,
  gate = makeGate,
  hellhound = default(Hellhound),
  gun1 = default(Gun1),
  upgrade = default(Upgrade),
  deveil = default(Deveil),
  exit = makeExit,
}



function makeEntity(data, state)
  local f = entityFactories[data.type]
  if f then
    local e = f(data, state)
    e:warpTo(data.x, data.y)
    if data.name ~= "" then
      e.name = data.name
    end
    e:prepare()
    return e
  else
    error("Unknown entity type " .. data.type)
  end
end
