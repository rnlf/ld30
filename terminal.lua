require 'key'

Terminal = Entity:extend()

function Terminal:new(state, i)
  Entity.new(self, state)

  self:sprite("data/terminal.png", 16, 22, {
    {'deny', {0 + 2 * (i-1)}, 1},
    {'ok', {1 + 2 * (i-1)}, 1}
  })

  self.shape = state.collider:addRectangle(0,0,self.size.x, self.size.y)
  self.shape.entity = self

  self:play('deny')

  self.gravity = 0
  self.access = 0
  self.type = i

  self.layer = 7
end


function Terminal:use()
  if self.access == 0 then
    G.sounds.open:play()
    self.access = 1
    self:play('ok')
    self.state.text:add("Activated", self.pos.x + self.size.x / 2, self.pos.y - 10, 0, 0, -10)

    local tgt = self.state.entities:find(function(a)
      return a.name == self.target
    end)

    if tgt then
      tgt:open()
    end
  end
end
