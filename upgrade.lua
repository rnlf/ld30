require 'key'

Upgrade = Entity:extend()

function Upgrade:new(state)
  Entity.new(self, state)

  self:sprite("data/upgrade.png", 32, 21, {
    {'ok', {0}, 1},
    {'used', {1}, 1}
  })

  self.shape = state.collider:addRectangle(0,0,self.size.x, self.size.y)
  self.shape.entity = self

  self:play('ok')

  self.gravity = 0

  self.layer = 7
  self.used = 0
end


function Upgrade:useWith(other)

  if self.used == 0 and other:is(Gun1) then
    G.sounds.open:play()
    G.sounds.powerup:play()
    other:upgrade()
    self.used = 1
    self:play('used')
  end
end
