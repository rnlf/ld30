Level = Object:extend()

local normalizeImageFilename = function(filename)
  return string.gsub(filename, '../', 'data/')

end

function Level:new(state, mapfile)
  self.state = state
  self.map = love.filesystem.load(mapfile)()

  for _, e in pairs(self.map.layers) do
    if e.name == "center" then
      self.centerlayer = e
    elseif e.name == "entities" then
      self.entitylayer = e
    elseif e.name == "border" then
      self.viewports = {}
      for _, b in pairs(e.objects) do
        if b.name == "jessie" then
          self.viewports[0] = {x = b.x, y = b.y, width = b.width, height = b.height}
        elseif b.name == "jack" then
          self.viewports[1] = {x = b.x, y = b.y, width = b.width, height = b.height}
        end
      end
    end
  end

  self.tileset = self.map.tilesets[1]

  local filename = normalizeImageFilename(self.tileset.image)
  self.tiles = G.images:load(filename)

  local tilefilew = self.tiles:getWidth()
  local tilefileh = self.tiles:getHeight()

  local h = self.map.height
  local w = self.map.width
  local th = self.map.tilewidth
  local tw = self.map.tileheight
  self.quads = {}
  local ntw = self.tiles:getWidth() / tw
  local nth = self.tiles:getHeight() / th
  for r = 0, nth - 1 do
    for c = 0, ntw -1 do
      local q = G.quads:make(c * tw, r * th, tw, th, tilefilew, tilefileh)
      self.quads[r*ntw+c + 1] = q
    end
  end

  self.batch = love.graphics.newSpriteBatch(self.tiles, 4000)
  self.batch:bind()
  for r = 1, h do
    for c = 1, w do
      local idx = self:getTile(c,r)
      if idx ~= 0 then
        local q = self.quads[idx]
        self.batch:add(self.quads[idx], (c-1) * tw, (r-1)*th)
      end
    end
  end
  self.batch:unbind()
  self:buildCollisionMesh()
end


function Level:draw()
  love.graphics.clear()

  local w = self.map.width
  local h = self.map.height
  local tw = self.map.tilewidth
  local th = self.map.tileheight

  love.graphics.draw(self.batch)
end


function Level:update(dt)

end


function Level:getTile(x, y)
  if x < 1 or x > self.map.width or y < 1 or y > self.map.height then
    return 0
  else
    return self.centerlayer.data[(y-1)*self.map.width+x]
  end
end


function Level:buildCollisionMesh()
  local tw = self.map.tilewidth
  local th = self.map.tileheight

  self.shapes = {}

  -- Horizontal line segments
  for r = 1, self.map.height do
    local firstcolabove = 0
    local firstcolbelow = 0

    for c = 1, self.map.width do
      if self:getTile(c,r) > 0 and self:getTile(c, r - 1) <= 0 then
        if firstcolabove == 0 then
          firstcolabove = c
        end
      else
        if firstcolabove ~= 0 then
          self:addTopCollisionLine((firstcolabove-1) * tw, (r-1) * th, (c-1) * tw)
          firstcolabove = 0
        end
      end

      if self:getTile(c,r) > 0 and self:getTile(c, r + 1) <= 0 then
        if firstcolbelow == 0 then
          firstcolbelow = c
        end
      else
        if firstcolbelow ~= 0 then
          self:addBottomCollisionLine((firstcolbelow-1) * tw, (r-1) * th, (c-1) * tw)
          firstcolbelow = 0
        end
      end
    end
  end

  -- Vertical line segments
  for c = 1, self.map.width do
    local firstrowleft = 0
    local firstrowright = 0

    for r = 1, self.map.height do
      if self:getTile(c,r) > 0 and self:getTile(c - 1, r) <= 0 then
        if firstrowleft == 0 then
          firstrowleft = r
        end
      else
        if firstrowleft ~= 0 then
          self:addLeftCollisionLine((c-1) * tw, (firstrowleft-1) * th, (r-1) * th)
          firstrowleft = 0
        end
      end

      if self:getTile(c,r) > 0 and self:getTile(c + 1, r) <= 0 then
        if firstrowright == 0 then
          firstrowright = r
        end
      else
        if firstrowright ~= 0 then
          self:addRightCollisionLine((c-1) * tw, (firstrowright-1) * th, (r-1) * th)
          firstrowright = 0
        end
      end

    end
  end
end

function Level:addLeftCollisionLine(x1,y1,y2)
  local tw = self.map.tilewidth
  local th = self.map.tileheight
  local shape = {x1,y1+1,
                 x1,y2-1,
                 x1+tw/2,y2-th/2,
                 x1+tw/2,y1+th/2}
  if y2 == y1 + th then
    shape = {x1,y1+1,
             x1,y2-1,
             x1+tw/2,y1+th/2}
  end
  self:addCollisionShape(shape)
end

function Level:addRightCollisionLine(x1,y1,y2)
  local tw = self.map.tilewidth
  local th = self.map.tileheight
  local shape = {x1+tw,y1+1,
                 x1+tw,y2-1,
                 x1+tw/2,y2-th/2,
                 x1+tw/2,y1+th/2}
  if y2 == y1 + th then
    shape = {x1+tw,y1+1,
             x1+tw,y2-1,
             x1+tw/2,y1+th/2}
  end
  self:addCollisionShape(shape)
end

function Level:addTopCollisionLine(x1,y1,x2)
  local tw = self.map.tilewidth
  local th = self.map.tileheight
  local shape = {x1+1,y1,
                 x1+tw/2,y1+th/2,
                 x2-tw/2,y1+th/2,
                 x2-1, y1}
  if x2 == x1 + tw then
    shape = {x1+1,y1,
             x1+tw/2,y1+th/2,
             x2-1, y1}
                            
  end
  self:addCollisionShape(shape)
end

function Level:addBottomCollisionLine(x1,y1,x2)
  local tw = self.map.tilewidth
  local th = self.map.tileheight
  local shape = {x1+1,     y1 + th,
                 x2-1,     y1 + th,
                 x2 - tw/2, y1 + th/2,
                 x1 + tw/2, y1 + th/2}

  if x2 == x1 + tw then
    shape = {x1+1, y1 + th,
                   x2-1, y1 + th/2,
                   x1 +tw/2, y1+th/2}
  end
  self:addCollisionShape(shape)
end

function Level:addCollisionShape(shape)
  local tw = self.map.tilewidth
  local th = self.map.tileheight
  local shape = self.state.collider:addPolygon(unpack(shape))
  self.state.collider:addToGroup("level", shape)
  shape.islevel = true
  self.state.collider:setPassive(shape)
  self.shapes[shape] = shape
end
