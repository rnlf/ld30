Object = require 'classic.classic'
tools  = require 'lust.tools'
flux   = require 'flux.flux'
HC     = require 'HardonCollider'
EffectComposer = require 'lust.effect_composer'

require 'scaleeffect'
require 'imagecache'
require 'quadcache'
require 'entity'
require 'group'

require 'state'



function love.load()
  love.window.setTitle("Jessie and Jack")
  G = {}
  G.font = love.graphics.newFont("data/Volter__28Goldfish_29.ttf", 9)
  G.width = 200
  G.height = 150
  G.images = ImageCache()
  G.quads  = QuadCache()
  G.state = State('data/level/test.lua')
  local draw = function()
    G.state:draw()
  end
  G.postEffects = EffectComposer(draw)
  G.postEffects:add_post_effect(ScaleEffect())

  local sounds = {
    shoot = 'shoot',
    teleport = 'teleport',
    open = 'open',
    hit = 'hit',
    pickup = 'pickup',
    drop = 'drop',
    fireball = 'fireball',
    fireball_hit = 'fireball_hit',
    jump = 'jump',
    hurt = 'hurt',
    killhellhound = 'killhellhound',
    killdeveil = 'killdeveil',
    powerup = 'powerup',
    gate = 'gate',
    die = 'die',
    exit = 'exit',
    warp = 'warp'
  }

  G.sounds = { }
  for n,f in pairs(sounds) do
    G.sounds[n] = love.audio.newSource("data/snd/" .. f .. ".ogg", 'static')
  end

  G.music = love.audio.newSource('data/snd/dammit.ogg', 'static')
  G.music:setVolume(0.75)
  G.music:setLooping(true)
  G.music:play()
  love.audio.setVolume(0.6)


end




function love.draw()
  G.postEffects:draw()
end


function love.update(dt)
  if love.keyboard.isDown('r') then
    G.state = State('data/level/test.lua')
  end
  G.state:update(dt)
end
